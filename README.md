This plugin adds a task which will delete old database backups.

# Installation
Just upload the content of the Upload/ directory to the root directory of your forum and activate the plugin

# Settings
Actually, only one setting exists: the minimum age of the file.

You can also modify the task parameters.