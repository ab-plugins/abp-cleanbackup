<?php
$l['abp_cbackname'] = 'ABP Clean Backup';
$l['abp_cbackdesc'] = 'Remove old database backups';
$l['abp_cback_setting_title'] = 'Clean Backup settings';
$l['abp_cback_setting_desc'] = 'Settings for the task';
$l['abp_cback_delay'] = 'Minimal age';
$l['abp_cback_delay_desc'] = 'Suppress files older than this value (in days)';
$l['abp_cback_cantwrite'] = 'Cannot write in directory {1}';
$l['abp_cback_ran'] = 'ABP Clean Backup ended : {1} file(s) deleted';