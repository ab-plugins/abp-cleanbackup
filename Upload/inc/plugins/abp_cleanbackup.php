<?php

/**
 * Clean backup task
 * (c) CrazyCat 2019
 */
if (!defined('IN_MYBB')) {
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

define('CN_ABCBACK', str_replace('.php', '', basename(__FILE__)));

function abp_cleanbackup_info() {
    global $lang;
    $lang->load(CN_ABCBACK);
    return array(
        'name' => $lang->abp_cbackname,
        'description' => $lang->abp_cbackdesc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-cleanbackup',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.1',
        'compatibility' => '18*',
        'codename' => CN_ABCBACK
    );
}

function abp_cleanbackup_activate() {
    global $db, $lang;
    $settinggroups = [
        'name' => CN_ABCBACK,
        'title' => $lang->abp_cback_setting_title,
        'description' => $lang->abp_cback_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'name' => CN_ABCBACK . '_delay',
            'title' => $lang->abp_cback_delay,
            'description' => $lang->abp_cback_delay_desc,
            'optionscode' => 'numeric\nmin=7',
            'value' => '30',
            'disporder' => 1,
            'gid' => $gid,
        ]
    ];
    foreach ($settings as $setting) {
        $db->insert_query('settings', $setting);
    }
    rebuild_settings();
    require_once MYBB_ROOT . "inc/functions_task.php";
    $task = [
        'title' => $lang->abp_cbackname,
        'description' => $lang->abp_cbackdesc,
        'file' => CN_ABCBACK,
        'minute' => '37',
        'hour' => '4',
        'day' => '*',
        'month' => '*',
        'weekday' => '*',
        'enabled' => 1,
        'logging' => 1,
        'locked' => 0
    ];
    $task_insert['nextrun'] = fetch_next_run($task);
    $db->insert_query("tasks", $task);
}

function abp_cleanbackup_deactivate() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABCBACK . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABCBACK . "'");
    $db->delete_query('tasks', "file='" . CN_ABCBACK . "'");
    rebuild_settings();
}
