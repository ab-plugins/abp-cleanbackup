<?php
/**
 * Task to delete old DB backups
 * (c) CrazyCat 2019
 */

define('CN_ABCBACK', str_replace('.php', '', basename(__FILE__)));

function task_abp_cleanbackup($task) {
    global $mybb, $lang, $config;
    $lang->load('abp_cleanbackup');

    if (!defined('MYBB_ADMIN_DIR')) {
        if (!isset($config['admin_dir'])) {
            $config['admin_dir'] = "admin";
        }
        define('MYBB_ADMIN_DIR', MYBB_ROOT . $config['admin_dir'] . DIRECTORY_SEPARATOR);
    }
    define('MYBB_BACKUP_DIR', MYBB_ADMIN_DIR . 'backups');

    if (!is_writable(MYBB_BACKUP_DIR)) {
        add_task_log($task, $lang->sprintf($lang->abp_cback_cantwrite, MYBB_BACKUP_DIR));
    } else {
        $ext = '.sql';
        if (function_exists('gzopen')) {
            $ext .= '.gz';
        }
        $backups = glob(MYBB_BACKUP_DIR . DIRECTORY_SEPARATOR . '*' . $ext);
        $cpt = 0;
        $age = $mybb->settings[CN_ABCBACK . '_delay'] * (24 * 60 * 60);
        foreach ($backups as $file) {
            $dmod = filemtime($file);
            if ((TIME_NOW - $dmod) > $age) {
                $cpt++;
                unlink($file);
            }
        }
        add_task_log($task, $lang->sprintf($lang->abp_cback_ran, $cpt));
    }
}
